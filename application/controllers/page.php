<?php

Class Page extends CI_Controller{
    function __construct(){
        parent::__construct();
        if($this->session->userdata('logged_in') !== TRUE){
          redirect('login');
        }
      }

      function index(){
        //Allowing akses to admin only
          if($this->session->userdata('level')==='1'){
              $this->load->view('home/index');
          }else{
              echo "Access Denied";
          }
     
      }
}