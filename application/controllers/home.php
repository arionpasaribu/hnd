<?php

class Home extends CI_Controller {

    function __construct(){
		parent::__construct();
	
		if($this->session->userdata('logged_in') != TRUE){
			redirect(base_url("login"));
		}
	}
    
    public function index ()
        {   
            // $this->load->model('w_wokjul');
            $data['wokjul']= $this->w_wokjul->get_data();
            $this->load->view('home/index',$data);

        }

    public function tambah ()
        {   
            
            $this->load->view('tambah');

        }


    public function wo_incoming ()
        {   
            // $this->load->model('w_wokjul');
            $data['WO_KEGIATAN_HND']= $this->w_wokjul->get_data();
            $this->load->view('home/wo_incoming',$data);

        } 
    
    public function rincian ()
        {
            $data['rincian']=$this->w_wokjul->rincian()->result_array();
            $this->load->view('home/rincian',$data);
    
        }

    public function rincian_out ()
        {
            $data['rincian_out']=$this->w_wokjul->rincian_out()->result_array();
            $this->load->view('home/rincian_out',$data);
    
        }
     
    public function rekapitulasi () 
     {
     	$data['rekapitulasi']= $this->w_wokjul->rekapitulasi()->result_array();
        $this->load->view('home/rekapitulasi', $data);

     }

    public function outgoing () 
     {
        $data['outgoing']= $this->w_wokjul->outgoing()->result_array();
        
        $this->load->view('home/outgoing', $data);

     }

    public function workorder()
        {
            // $data['customer']=$this->w_wokjul->get_customer();
            $data = [
                'customer' => $this->w_wokjul->get_customer(),
                'jasa' => $this->w_wokjul->get_jasa(),
            ];
            $this->load->view('home/workorder',$data);
        }
        

    public function databarang () 
     {
        $data['databarang']= $this->w_wokjul->databarang()->result();
        $this->load->view('home/databarang', $data);

     }

     public function tambah_data()
     {
         $kd  = $this->input->post('kode_barang');
         $nama= $this->input->post('nama_barang');
         $satuan  = $this->input->post('satuan');
         $kemasan  = $this->input->post('kemasan');
         $tarif  = $this->input->post('tarif');
         $pnbp  = $this->input->post('pnbp');
         $td  = $this->input->post('tarif_dasar');
         $mu  = $this->input->post('mata_uang');
         $status  = $this->input->post('status');
         $grup = $this->input->post('grup');

         $data = array(
            'kode_barang'   => $kd,
            'nama_barang'   => $nama,
            'satuan'   => $satuan,
            'kemasan'   => $kemasan,
            'tarif'   => $tarif,
            'pnbp'   => $pnbp,
            'tarif_dasar'   => $td,
            'mata_uang'   => $mu,
            'status'   => $status,
            'grup'   => $grup,
         );
         $this->w_wokjul->tambah_data($data, 'barang_hnd');
         redirect('home/databarang');

    }
    
    public function edit($id)
    {
        $where = array('kode_barang' =>$id);
        $data['databarang'] = $this->w_wokjul->edit_data($where, 'barang_hnd')->result( );
        $this->load->view('home/editbarang', $data);


    }

    public function update(){
         $kd  = $this->input->post('kode_barang');
         $nama= $this->input->post('nama_barang');
         $satuan  = $this->input->post('satuan');
         $kemasan  = $this->input->post('kemasan');
         $tarif  = $this->input->post('tarif');
         $pnbp  = $this->input->post('pnbp');
         $td  = $this->input->post('tarif_dasar');
         $mu  = $this->input->post('mata_uang');
         $status  = $this->input->post('status');
         $grup = $this->input->post('grup');

         $data = array(
            'nama_barang'   => $nama,
            'satuan'   => $satuan,
            'kemasan'   => $kemasan,
            'tarif'   => $tarif,
            'pnbp'   => $pnbp,
            'tarif_dasar'   => $td,
            'mata_uang'   => $mu,
            'status'   => $status,
            'grup'   => $grup,
         );

         $where = array(
             'kode_barang' => $kd
         );

         $this->w_wokjul->update_data($where, $data, 'barang_hnd');
         redirect('home/databarang');

    }

    public function delete($id)
    {
        // $id = $_POST['KODE_BARANG'];
		// $result = $this->w_wokjul->delete($id);
        $where = array('kode_barang'=>$id);
        $this->w_wokjul->delete_data($where,'barang_hnd');
        redirect('home/databarang');

    }

    public function datacustomer () 
     {
        $data['datacustomer']= $this->w_wokjul->datacustomer()->result();
        $this->load->view('home/datacustomer', $data);

     }

     public function tambah_datacustomer()
     {
         $kc = $this->input->post('kode_customer');
         $nama= $this->input->post('nama_customer');
         $alamat  = $this->input->post('alamat');
         $kota  = $this->input->post('kota');
         $negara  = $this->input->post('negara');
         $jenis  = $this->input->post('jenis_customer');
         $cp  = $this->input->post('kontak_person');
         $tgl_bergabung  = $this->input->post('tgl_bergabung');
         $tgl_keluar = $this->input->post('tgl_keluar');
         $npwp = $this->input->post('npwp');
         $status  = $this->input->post('status');
         $author = $this->input->post('author');
         $set_time = $this->input->post('set_time');
         $no_telp = $this->input->post('no_telp');

         $data = array(
            'kode_customer'   => $kc,
            'nama_customer'   => $nama,
            'alamat'   => $alamat,
            'kota'   => $kota,
            'negara'   => $negara,
            'jenis_customer'   => $jenis,
            'kontak_person'   => $cp,
            'tgl_bergabung'   => $tgl_bergabung,
            'tgl_keluar'   => $tgl_keluar,
            'npwp'   => $npwp,
            'status'   => $status,
            'author'   => $author,
            'set_time'   => $set_time,
            'no_telp'   => $no_telp,
         );
         $this->w_wokjul->tambah_datacustomer($data, 'customer_hnd');
         redirect('home/datacustomer');

    }
    
   public function editcustomer($id)
    {
        $where = array('kode_customer' =>$id);
        $data['datacustomer'] = $this->w_wokjul->edit_datacustomer($where, 'customer_hnd')->result( );
        $this->load->view('home/editcustomer', $data);


    }

    public function updatecustomer(){
         $kc = $this->input->post('kode_customer');
         $nama= $this->input->post('nama_customer');
         $alamat  = $this->input->post('alamat');
         $kota  = $this->input->post('kota');
         $negara  = $this->input->post('negara');
         $jenis  = $this->input->post('jenis_customer');
         $cp  = $this->input->post('kontak_person');
         $tgl_bergabung  = $this->input->post('tgl_bergabung');
         $tgl_keluar = $this->input->post('tgl_keluar');
         $npwp = $this->input->post('npwp');
         $status  = $this->input->post('status');
         $author = $this->input->post('author');
         $set_time = $this->input->post('set_time');
         $no_telp = $this->input->post('no_telp');

         $data = array(
            'kode_customer'   => $kc,
            'nama_customer'   => $nama,
            'alamat'   => $alamat,
            'kota'   => $kota,
            'negara'   => $negara,
            'jenis_customer'   => $jenis,
            'kontak_person'   => $cp,
            'tgl_bergabung'   => $tgl_bergabung,
            'tgl_keluar'   => $tgl_keluar,
            'npwp'   => $npwp,
            'status'   => $status,
            'author'   => $author,
            'set_time'   => $set_time,
            'no_telp'   => $no_telp,
         );

         $where = array(
             'kode_customer' => $kc
         );

         $this->w_wokjul->update_datacustomer($where, $data, 'customer_hnd');
         redirect('home/datacustomer');

    }

    public function deletecustomer($id)
    {
        // $id = $_POST['KODE_CUSTOMER'];
        // $result = $this->w_wokjul->delete($id);
        $where = array('kode_customer'=>$id);
        $this->w_wokjul->delete_datacustomer($where,'customer_hnd');
        redirect('home/datacustomer');

    }
    
        public function datajasa () 
     {
        $data['datajasa']= $this->w_wokjul->datajasa()->result();
        $this->load->view('home/datajasa', $data);

     }

     public function tambah_datajasa()
     {
         $kj  = $this->input->post('kode_jasa');
         $nama= $this->input->post('nama_jasa');
         $nomor_acount  = $this->input->post('nomor_acount');
         $status  = $this->input->post('status');
         $author  = $this->input->post('author');
         $set_time = $this->input->post('set_time');

         $data = array(
            'kode_jasa'   => $kj,
            'nama_jasa'   => $nama,
            'nomor_acount'   => $nomor_acount,
            'status'   => $status,
            'author'   => $author,
            'set_time'   => $set_time,
         );
         $this->w_wokjul->tambah_datajasa($data, 'jenis_jasa_hnd');
         redirect('home/datajasa');

    }
    
    public function editjasa($id)
    {
        $where = array('kode_jasa' =>$id);
        $data['datajasa'] = $this->w_wokjul->edit_datajasa($where, 'jenis_jasa_hnd')->result( );
        $this->load->view('home/editjasa', $data);


    }

    public function updatejasa(){
         $kj  = $this->input->post('kode_jasa');
         $nama= $this->input->post('nama_jasa');
         $nomor_acount  = $this->input->post('nomor_acount');
         $status  = $this->input->post('status');
         $author  = $this->input->post('author');
         $set_time = $this->input->post('set_time');

         $data = array(
            'kode_jasa'   => $kj,
            'nama_jasa'   => $nama,
            'nomor_acount'   => $nomor_acount,
            'status'   => $status,
            'author'   => $author,
            'set_time'   => $set_time,
         );

         $where = array(
             'kode_jasa' => $kj
         );

         $this->w_wokjul->update_datajasa($where, $data, 'jenis_jasa_hnd');
         redirect('home/datajasa');

    }

    public function deletejasa($id)
    {
        // $id = $_POST['KODE_BARANG'];
        // $result = $this->w_wokjul->delete($id);
        $where = array('kode_jasa'=>$id);
        $this->w_wokjul->delete_datajasa($where,'jenis_jasa_hnd');
        redirect('home/datajasa');

    }

    public function databandara () 
     {
        $data['databandara']= $this->w_wokjul->databandara()->result();
        $this->load->view('home/databandara', $data);

     }

     public function tambah_databandara()
     {
         $ka  = $this->input->post('kode_asal_tujuan');
         $nama= $this->input->post('nama_asal_tujuan');
         $inisial  = $this->input->post('inisial');
         $negara  = $this->input->post('negara');
         $kota  = $this->input->post('kota');

         $data = array(
            'kode_asal_tujuan'   => $ka,
            'nama_asal_tujuan'   => $nama,
            'inisial'   => $inisial,
            'negara'   => $negara,
            'kota'   => $kota,
         );
         $this->w_wokjul->tambah_databandara($data, 'kota_hnd');
         redirect('home/databandara');

    }
    
    public function editbandara($id)
    {
        $where = array('kode_asal_tujuan' =>$id);
        $data['databandara'] = $this->w_wokjul->edit_databandara($where, 'kota_hnd')->result( );
        $this->load->view('home/editbandara', $data);


    }

    public function updatebandara(){
         $ka  = $this->input->post('kode_asal_tujuan');
         $nama= $this->input->post('nama_asal_tujuan');
         $inisial  = $this->input->post('inisial');
         $negara  = $this->input->post('negara');
         $kota  = $this->input->post('kota');

        $data = array(
            'kode_asal_tujuan'   => $kd,
            'nama_asal_tujuan'   => $nama,
            'inisial'   => $inisial,
            'negara'   => $negara,
            'kota'   => $kota,
         );

         $where = array(
             'kode_asal_tujuan' => $ka
         );

         $this->w_wokjul->update_databandara($where, $data, 'kota_hnd');
         redirect('home/databandara');

    }

    public function deletebandara($id)
    {
        // $id = $_POST['KODE_BARANG'];
        // $result = $this->w_wokjul->delete($id);
        $where = array('kode_asal_tujuan'=>$id);
        $this->w_wokjul->delete_data($where,'kota_hnd');
        redirect('home/databandara');

    }

    public function datapenerbangan () 
     {
        $data['datapenerbangan']= $this->w_wokjul->datapenerbangan()->result();
        $this->load->view('home/datapenerbangan', $data);

     }

     public function tambah_datapenerbangan()
     {
         $kk  = $this->input->post('kode_kapal');
         $nama= $this->input->post('nama_kapal');
         $bendera  = $this->input->post('bendera');
         $status  = $this->input->post('status');
         $author  = $this->input->post('author');
         $set_time  = $this->input->post('set_time');
         $inisial  = $this->input->post('inisial');

         $data = array(
            'kode_kapal'   => $kk,
            'nama_kapal'   => $nama,
            'bendera'   => $bendera,
            'status'   => $status,
            'author'   => $author,
            'set_time'   => $set_time,
            'inisial'   => $inisial,
         );
         $this->w_wokjul->tambah_datapenerbangan($data, 'kapal_hnd');
         redirect('home/datapenerbangan');

    }
    
    public function editpenerbangan($id)
    {
        $where = array('kode_kapal' =>$id);
        $data['datapenerbangan'] = $this->w_wokjul->edit_datapenerbangan($where, 'kapal_hnd')->result( );
        $this->load->view('home/editpenerbangan', $data);


    }

    public function updatepenerbangan()
    {
         $kk  = $this->input->post('kode_kapal');
         $nama= $this->input->post('nama_kapal');
         $bendera  = $this->input->post('bendera');
         $status  = $this->input->post('status');
         $author  = $this->input->post('author');
         $set_time  = $this->input->post('set_time');
         $inisial  = $this->input->post('inisial');

         $data = array(
            'kode_kapal'   => $kk,
            'nama_kapal'   => $nama,
            'bendera'   => $bendera,
            'status'   => $status,
            'author'   => $author,
            'set_time'   => $set_time,
            'inisial'   => $inisial,
         );

         $where = array(
             'kode_kapal' => $kk
         );

         $this->w_wokjul->update_datapenerbangan($where, $data, 'kapal_hnd');
         redirect('home/datapenerbangan');

    }

    public function deletepenerbangan($id)
    {
        // $id = $_POST['KODE_BARANG'];
        // $result = $this->w_wokjul->delete($id);
        $where = array('kode_kapal'=>$id);
        $this->w_wokjul->delete_data($where,'kapal_hnd');
        redirect('home/datapenerbangan');

    }

    public function datasmu()
        {
            $data = [
                
                'nomorwo' => $this->w_wokjul->get_nomorwo(),
                
                'tarif' => $this->w_wokjul->get_tarif(),

                'namakapal' => $this->w_wokjul->get_namakapal(),

                'namakota' => $this->w_wokjul->get_namakota(),

                'namacustomer' => $this->w_wokjul->get_namacustomer(),



            ];
            $this->load->view('home/datasmu',$data);
             
        }

    
        public function tambah_datasmu()
        {
            $smu  = $this->input->post('smu');
            $telly = $this->input->post('telly');
            $nb  = $this->input->post('nama_barang');
            $jk  = $this->input->post('jumlah_koli');
            $ps  = $this->input->post('pada_smu');
            $ditagih = $this->input->post('ditagih');
            $hari  = $this->input->post('hari');
            $pnbp  = $this->input->post('pnbp');
            $tarif  = $this->input->post('tarif');
            $tk = $this->input->post('tgl_kapal');
            $jkt = $this->input->post('tgl_kapal');
            $np = $this->input->post('no_penerbangan');
            $kat = $this->input->post('kode_asal_tujuan');
            $pemilik = $this->input->post('pemilik');



            
   
            $data = array(
               'nomor_smu'   => $smu,
               'nama_barang'   => $nb,
               'qty_barang'   => $jk,
               'kg_barang'   => $ps,
               'kg_telly'   => $ditagih,
               'jhari'   => $hari,
               'pnbp'   => $pnbp,
               'tarif'   => $tarif,
               'tgl_kapal' => $tk,
               'jam_kapal' => $jkt,
               'no_penerbangan' => $np,
               'kode_asal_tujuan' => $kat,
               'pemilik' => $pemilik,


               
            );
            $this->w_wokjul->tambah_datasmu($data, 'WO_KEGIATAN_HND');
            redirect('home/datasmu');
   
       }

}