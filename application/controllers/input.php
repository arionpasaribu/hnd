<?php

class Input extends CI_Controller {

    public function insert_work_order(){
        $no_wo  = $this->input->post('nomor_wo');
        $tgl= $this->input->post('tgl_wo');
        $kd_kustomer  = $this->input->post('kode_customer');
        $kd_jasa  = $this->input->post('kode_jasa');
        $jenis  = $this->input->post('jenis');
        $no_nota  = $this->input->post('nomor_nota');
        $idr = 'idr';

        $data = array(
           'NOMOR_WO'   => $no_wo,
           'TGL_WO'   => $tgl,
           'NOMOR_NOTA'   => $no_nota,
           'KODE_CUSTOMER'   => $kd_kustomer,
           'KODE_JASA'   => $kd_jasa,
           'JENIS_BAYAR'   => $jenis,
           'MATA_UANG'   => $idr,
           'STATUS'   => $jenis,
           'SET_TIME'   => $tgl,
        );
        $this->w_wokjul->tambah_data_wo($data, 'barang_hnd');
        redirect('home/datasmu');

    }

}