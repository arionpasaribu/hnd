<?php 

class W_wokjul extends CI_Model
{
    public function get_data()
    {

        $this->db->select('*');
        $this->db->from('WOKJUL');
        $this->db->limit(50);
        $query = $this->db->get();
        if( $query->num_rows() > 0 )
        {
            return $query->result_array();
        }
      
    }  

    public function rincian ()
    {
        return $this->db->query("SELECT 
                CASE WO.JENIS_WO 
                    WHEN 'INDOM' THEN 'DOMESTIC CARGO'
                    WHEN 'ININT' THEN 'INTERNATIONAL CARGO' 
                END JENIS_CARGO,
                CASE WO.JENIS_WO 
                    WHEN 'INDOM' THEN 'INCOMING DOMESTIC'
                    WHEN 'ININT' THEN 'INCOMING INTERNATIONAL'
                END JENIS_WO,
                WO.JENIS_BAYAR,
                WO.NOMOR_WO,
                WO.TGL_WO TGL_WO,
                WO.NAMA_CUSTOMER,
                WK.NOMOR_SMU,
                KP.NAMA_KAPAL,
                KT.NAMA_ASAL_TUJUAN,
                WK.NAMA_BARANG,
                WK.QTY_BARANG,
                WK.SATUAN_QTY,
                WK.KG_BARANG,
                WK.TARIF-WK.PNBP TARIF_DASAR,
                WK.PNBP,
                WO.MATA_UANG,
                WK.TARIF,
                WK.PNBP*WK.KG_BARANG JML_PNBP,
                (WK.TARIF-WK.PNBP)*WK.KG_BARANG JML_TARIF_DASAR,
                WK.JML_TARIF
        FROM WORK_ORDER_HND WO,
                KAPAL_HND KP,
                KOTA_HND KT,
                WO_KEGIATAN_HND WK
        WHERE ((WO.JENIS_WO = 'INDOM') OR (WO.JENIS_WO = 'ININT'))
                AND WK.NOMOR_WO=WO.NOMOR_WO
                AND WK.KODE_KAPAL=KP.KODE_KAPAL
                AND WK.KODE_ASAL_TUJUAN=KT.KODE_ASAL_TUJUAN
                AND ((WO.STATUS='NOTA') OR (WO.STATUS='CASH'))
                AND WK.STATUS <> 'BATAL'
        ORDER BY WO.NOMOR_WO
        limit 50;");
    }

    public function rincian_out ()
    {
        return $this->db->query("SELECT 
                CASE WO.JENIS_WO 
                    WHEN 'OUTDOM' THEN 'DOMESTIC CARGO'
                    WHEN 'OUTINT' THEN 'INTERNATIONAL CARGO' 
                END JENIS_CARGO,
                CASE WO.JENIS_WO 
                    WHEN 'OUTDOM' THEN 'OUT GOING DOMESTIC'
                    WHEN 'OUTINT' THEN 'OUT GOING INTERNATIONAL'
                END JENIS_WO,
                WO.JENIS_BAYAR,
                WO.NOMOR_WO,
                WO.TGL_WO TGL_WO,
                WO.NAMA_CUSTOMER,
                WK.NOMOR_SMU,
                KP.NAMA_KAPAL,
                KT.NAMA_ASAL_TUJUAN,
                WK.NAMA_BARANG,
                WK.QTY_BARANG,
                WK.SATUAN_QTY,
                WK.KG_BARANG,
                WK.TARIF-WK.PNBP TARIF_DASAR,
                WK.PNBP,
                WO.MATA_UANG,
                WK.TARIF,
                WK.PNBP*WK.KG_BARANG JML_PNBP,
                (WK.TARIF-WK.PNBP)*WK.KG_BARANG JML_TARIF_DASAR,
                WK.JML_TARIF
        FROM WORK_ORDER_HND WO,
                KAPAL_HND KP,
                KOTA_HND KT,
                WO_KEGIATAN_HND WK
        WHERE ((WO.JENIS_WO = 'OUTDOM') OR (WO.JENIS_WO = 'OUTINT'))
                AND WK.NOMOR_WO=WO.NOMOR_WO
                AND WK.KODE_KAPAL=KP.KODE_KAPAL
                AND WK.KODE_ASAL_TUJUAN=KT.KODE_ASAL_TUJUAN
                AND ((WO.STATUS='NOTA') OR (WO.STATUS='CASH'))
                AND WK.STATUS <> 'BATAL'
        ORDER BY WO.NOMOR_WO
        limit 50;");
    }

    public function rekapitulasi()
    {
    	return $this->db->query("SELECT 
			       CASE WO.JENIS_WO 
			           WHEN 'INDOM' THEN 'DOMESTIC CARGO'
			           ELSE 'INTERNATIONAL CARGO' 
			       END JENIS_BAYAR_,
			       CASE WO.JENIS_WO 
			           WHEN 'INDOM' THEN 'INCOMING DOMESTIC'
			           WHEN 'ININT' THEN 'INCOMING INTERNATIONAL'
			       END JENIS_WO_,
			       CASE WO.JENIS_WO 
			           WHEN 'INDOM' THEN BR.NAMA_BARANG
			           WHEN 'OUTDOM' THEN BR.NAMA_BARANG 
			           ELSE WK.PEMILIK 
			       END NAMA_BARANG_,
			       SUM(WK.QTY_BARANG) QTY_BARANG,
			       SUM(WK.KG_BARANG) KG_BARANG,
			       SUM(WK.PNBP*WK.KG_BARANG) JML_PNBP,
			       SUM((27.5/100)*((WK.TARIF-WK.PNBP)*WK.KG_BARANG)) OB,
			       SUM((72.5/100)*((WK.TARIF-WK.PNBP)*WK.KG_BARANG)) PERSERO,
			       SUM(WK.JML_TARIF) JML_TARIF
			 FROM WORK_ORDER_HND WO,
			       BARANG_HND BR,
			       WO_KEGIATAN_HND WK,
			       KAPAL_HND KP
			 WHERE ((WO.JENIS_WO = 'INDOM') OR (WO.JENIS_WO = 'ININT'))
			   AND WK.NOMOR_WO=WO.NOMOR_WO
			   AND WK.KODE_BARANG=BR.KODE_BARANG
			   AND WK.KODE_KAPAL=KP.KODE_KAPAL
			   AND ((WO.STATUS='NOTA') OR (WO.STATUS='CASH'))
			   AND WK.STATUS <> 'BATAL'
			GROUP BY JENIS_BAYAR_, JENIS_WO_, NAMA_BARANG_");
    }
        public function outgoing()
    {
    	return $this->db->query("SELECT 
			       CASE WO.JENIS_WO
			           WHEN 'OUTDOM' THEN 'DOMESTIC CARGO' 
			           ELSE 'INTERNATIONAL CARGO' 
			       END JENIS_BAYAR_,
			       CASE WO.JENIS_WO 
			           WHEN 'OUTDOM' THEN 'OUT GOING DOMESTIC' 
			           WHEN 'OUTINT' THEN  'OUT GOING INTERNATIONAL' 
			       END JENIS_WO_,
			       CASE WO.JENIS_WO 
			           WHEN 'INDOM' THEN BR.NAMA_BARANG
			           WHEN 'OUTDOM' THEN BR.NAMA_BARANG 
			           ELSE WK.PEMILIK 
			       END NAMA_BARANG_,
			       SUM(WK.QTY_BARANG) QTY_BARANG,
			       SUM(WK.KG_BARANG) KG_BARANG,
			       SUM(WK.PNBP*WK.KG_BARANG) JML_PNBP,
			       SUM((27.5/100)*((WK.TARIF-WK.PNBP)*WK.KG_BARANG)) OB,
			       SUM((72.5/100)*((WK.TARIF-WK.PNBP)*WK.KG_BARANG)) PERSERO,
			       SUM(WK.JML_TARIF) JML_TARIF
			 FROM WORK_ORDER_HND WO,
			       BARANG_HND BR,
			       WO_KEGIATAN_HND WK,
			       KAPAL_HND KP
			 WHERE ((WO.JENIS_WO = 'OUTDOM') OR (WO.JENIS_WO = 'OUTINT'))
			   AND WK.NOMOR_WO=WO.NOMOR_WO
			   AND WK.KODE_BARANG=BR.KODE_BARANG
			   AND WK.KODE_KAPAL=KP.KODE_KAPAL
			   AND ((WO.STATUS='NOTA') OR (WO.STATUS='CASH'))
			   AND WK.STATUS <> 'BATAL'
			GROUP BY JENIS_BAYAR_, JENIS_WO_, NAMA_BARANG_");
    }

	public function databarang()

	{
		return $this->db->get_where('BARANG_HND', array('STATUS' => 'OPEN'));
	}

	public function tambah_data($data)
	{
		$this->db->insert('BARANG_HND', $data);
	}


	public function delete_data($where, $table)
	{
		$this->db->where($where);
		$this->db->delete($table);
	}

	public function edit_data($where,$table){
		return $this->db->get_where($table, $where);
	}
	
	public function update_data($where, $data, $table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}

	public function datacustomer()

	{
		return $this->db->get_where('CUSTOMER_HND',array('STATUS'=>'OPEN'));
	}

	public function tambah_datacustomer($data)
	{
		$this->db->insert('CUSTOMER_HND', $data);
	}


	public function delete_datacustomer($where, $table)
	{
		$this->db->where($where);
		$this->db->delete($table);
	}

	public function edit_datacustomer($where,$table){
		return $this->db->get_where($table, $where);
	}
	
	public function update_datacustomer($where, $data, $table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}

	public function datajasa()

	{
		return $this->db->get_where('JENIS_JASA_HND',array('STATUS'=>'OPEN'));
	}

	public function tambah_datajasa($data)
	{
		$this->db->insert('JENIS_JASA_HND', $data);
	}


	public function delete_datajasa($where, $table)
	{
		$this->db->where($where);
		$this->db->delete($table);
	}

	public function edit_datajasa($where,$table){
		return $this->db->get_where($table, $where);
	}
	
	public function update_datajasa($where, $data, $table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}

	public function databandara()

	{
		return $this->db->get('KOTA_HND');
	}

	public function tambah_databandara($data)
	{
		$this->db->insert('KOTA_HND', $data);
	}


	public function delete_databandara($where, $table)
	{
		$this->db->where($where);
		$this->db->delete($table);
	}

	public function edit_databandara($where,$table){
		return $this->db->get_where($table, $where);
	}
	
	public function update_databandara($where, $data, $table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}

	public function datapenerbangan()

	{
		return $this->db->get_where('KAPAL_HND',array('STATUS'=>'OPEN'));
	}

	public function tambah_datapenerbangan($data)
	{
		$this->db->insert('KAPAL_HND', $data);
	}


	public function delete_datapenerbangan($where, $table)
	{
		$this->db->where($where);
		$this->db->delete($table);
	}

	public function edit_datapenerbangan($where,$table){
		return $this->db->get_where($table, $where);
	}
	
	public function update_datapenerbangan($where, $data, $table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}

	public function get_customer()
	{
		$query = $this->db->query('SELECT * FROM CUSTOMER_HND');
    	return $query->result_array();
		
	}

	public function get_jasa()
	{
		$query = $this->db->query('SELECT * FROM JENIS_JASA_HND');
    	return $query->result_array();
		
	}

	public function tambah_data_wo($data)
	{
		$this->db->insert('WORK_ORDER_HND', $data);
	}


	public function tambah_datasmu($data)
	{
		$this->db->insert('WO_KEGIATAN_HND', $data);
	}

	public function get_nomorwo()
	{
		$query = $this->db->query('SELECT * FROM WORK_ORDER_HND');
    	return $query->result_array();
		
	}

	public function get_tarif()
	{
		$query = $this->db->query('SELECT * FROM WO_KEGIATAN_HND');
    	return $query->result_array();
		
	}

	public function get_namakapal()
	{
		$query = $this->db->query('SELECT * FROM KAPAL_HND');
    	return $query->result_array();
		
	}

	public function get_namakota()
	{
		$query = $this->db->query('SELECT * FROM KOTA_HND');
    	return $query->result_array();
		
	}

	public function get_namacustomer()
	{
		$query = $this->db->query('SELECT * FROM CUSTOMER_HND');
    	return $query->result_array();
		
	}


}