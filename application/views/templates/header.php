
    <div class="page-main-header">
        <div class="main-header-left">
            <div class="logo-wrapper">
                <a href="index.html">
                    <img src="<?php echo base_url() ?>template/assets/images/logo-pb.png" class="image-dark" alt=""/>
                    <img src="<?php echo base_url() ?>template/assets/images/logo-light-dark-layout.png" class="image-light" alt=""/>
                </a>
            </div>
        </div>
        <div class="main-header-right row">
            <div class="mobile-sidebar">
                <div class="media-body text-right switch-sm">
                    <label class="switch">
                        <input type="checkbox" id="sidebar-toggle" checked>
                        <span class="switch-state"></span>
                    </label>
                </div>
            </div>
        </div>
    </div>
    <div class="page-body-wrapper">

    
    