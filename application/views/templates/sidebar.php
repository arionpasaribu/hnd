        <div class="page-sidebar custom-scrollbar">
            <div class="sidebar-user text-center">
                <div>
                    <img class="img-50 rounded-circle" src="<?php echo base_url () ?>template/assets/images/user/1.jpg" alt="#">
                </div>
                <h6 class="mt-3 f-12">User</h6>
            </div>
            <ul class="sidebar-menu">
                <li class="active">
                            <a href="#" class="sidebar-header">
                                    <i class="icon-blackboard"></i><span>Input Data</span>
                                    <i class="fa fa-angle-right pull-right"></i>
                            </a>
                        <ul class="sidebar-submenu">
                            <li><a href="<?php echo base_url('home/workorder') ?>"><i class="fa fa-angle-right"></i>Work Order</a></li>
                            <li><a href="<?php echo base_url('home/datasmu') ?>"><i class="fa fa-angle-right"></i>Entry Data SMU</a></li>
                            <li><a href="<?php echo base_url('home/datacustomer') ?>"><i class="fa fa-angle-right"></i>Entry Data Customer</a></li>
                            <li><a href="<?php echo base_url('home/databarang') ?>"><i class="fa fa-angle-right"></i>Entry Data Barang</a></li>
                            <li><a href="<?php echo base_url('home/datajasa') ?>"><i class="fa fa-angle-right"></i>Entry Data Jenis Jasa</a></li>
                            <li><a href="<?php echo base_url('home/databandara') ?>"><i class="fa fa-angle-right"></i>Entry Data Bandara</a></li>
                            <li><a href="<?php echo base_url('home/datapenerbangan') ?>"><i class="fa fa-angle-right"></i>Entry Data Penerbangan</a></li>
                        </ul>
                </li>
                
                <li class="active">
                    <a href="#" class="sidebar-header">
                            <i class="icon-desktop"></i><span>LAPORAN</span>
                            <i class="fa fa-angle-right pull-right"></i>
                    </a>
                    <ul class="sidebar-submenu menu-open" style="display: block;">
                        <li><a href="<?php echo base_url('home/rincian') ?>"><i class="fa fa-angle-right"></i>Rincian Kegiatan Incoming</a></li>
                        <li><a href="<?php echo base_url('home/rincian_out') ?>"><i class="fa fa-angle-right"></i>Rincian Kegiatan Outgoing</a></li>
                        <li><a href="<?php echo base_url('home/rekapitulasi') ?>"><i class="fa fa-angle-right"></i>Rekapitulasi Kegiatan Incoming</a></li>
                        <li><a href="<?php echo base_url('home/outgoing') ?>"><i class="fa fa-angle-right"></i>Rekapitulasi Kegiatan Outgoing</a></li>
                        
                    </ul>
                </li>

                <li><a href="<?php echo base_url('login/logout') ?>">Logout</a></li>

                </div>
        

        
<!-- latest jquery-->
<script src="<?php echo base_url () ?>template/assets/js/jquery-3.2.1.min.js" ></script>

<!-- Bootstrap js-->
<script src="<?php echo base_url () ?>template/assets/js/bootstrap/popper.min.js" ></script>
<script src="<?php echo base_url () ?>template/assets/js/bootstrap/bootstrap.js" ></script>

<!-- Sidebar jquery-->
<script src="<?php echo base_url () ?>template/assets/js/sidebar-menu.js" ></script>
<script src="<?php echo base_url () ?>template/assets/js/chat-sidebar/chat.js"></script>

<!--Datatable js-->
<script src="<?php echo base_url () ?>template/assets/js/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url () ?>template/assets/js/datatables/datatable.custom.js"></script>

<!-- Theme js-->
<script src="<?php echo base_url () ?>template/assets/js/script.js" ></script>