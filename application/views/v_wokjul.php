<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>WOKJUL</title>
</head>
<body>
    <h2>Data WOKJUL</h2>

    <a href="<?php echo base_url('/home/tambah') ?>">Tambah Data</a>
    <br>
    <br>
    <table border="1" style="collapse: border-collapse">
        <tr>
            <th>NOMOR WO</th>
            <th>NOMOR SMU</th>
            <th>KODE KAPAL</th>
            <th>KODE ASAL TUJUAN</th>
            <th>KODE PEMILIK</th>
            <th>KODE BARANG</th>
            <th>QTY BARANG</th>
            <th>SATUAN QTY</th>
            <th>KG BARANG</th>
            <th>TARIF</th>
            <th>JUMLAH TARIF</th>
            <th>STATUS</th>
            <th>AUTHOR</th>
            <th>SET TIME</th>
            <th>DISCOUNT</th>
            <th>PNBP</th>
            <th>PEMILIK</th>
            <th>NAMA BARANG</th>
            <th>TANGGAL KAPAL</th>
            <th>NOMOR TELLY</th>
            <th>KG TELLY</th>
            <th>AUTHOR UPDATE</th>
            <th>SET TIME UPDATE</th>
            <th>NO PENERBANGAN</th>
            <th>QTY TELLY</th>
            <th>AKSI</th>
        </tr>
        <?php foreach ($wokjul as $key) : ?>
           <tr>
            <td><?php echo $key['NOMOR_WO'] ?></td>
            <td><?php echo $key['NOMOR_SMU'] ?></td>
            <td><?php echo $key['KODE_KAPAL'] ?></td>
            <td><?php echo $key['KODE_ASAL_TUJUAN'] ?></td>
            <td><?php echo $key['KODE_PEMILIK'] ?></td>
            <td><?php echo $key['KODE_BARANG'] ?></td>
            <td><?php echo $key['QTY_BARANG'] ?></td>
            <td><?php echo $key['SATUAN_QTY'] ?></td>
            <td><?php echo $key['KG_BARANG'] ?></td>
            <td><?php echo $key['TARIF'] ?></td>
            <td><?php echo $key['JML_TARIF'] ?></td>
            <td><?php echo $key['STATUS'] ?></td>
            <td><?php echo $key['AUTHOR'] ?></td>
            <td><?php echo $key['SET_TIME'] ?></td>
            <td><?php echo $key['DISCOUNT'] ?></td>
            <td><?php echo $key['PNBP'] ?></td>
            <td><?php echo $key['PEMILIK'] ?></td>
            <td><?php echo $key['NAMA_BARANG'] ?></td>
            <td><?php echo $key['TGL_KAPAL'] ?></td>
            <td><?php echo $key['NOMOR_TELLY'] ?></td>
            <td><?php echo $key['KG_TELLY'] ?></td>
            <td><?php echo $key['AUTHOR_UPDATE'] ?></td>
            <td><?php echo $key['SET_TIME_UPDATE'] ?></td>
            <td><?php echo $key['NO_PENERBANGAN'] ?></td>
            <td><?php echo $key['QTY_TELLY'] ?></td>
            <td><button><a href="<?php echo base_url('/home/edit') ?>">Edit</a></button><button><a href="">Delete</a></button></td>
           </tr>
        <?php endforeach ?>
    </table>
</body>
</html>