<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>WOKJUL</title>
</head>
<body>
	<h3>HALAMAN EDIT DATA</h3>
	<table>
		<tr>
			<td>NOMOR WO</td>
			<td>:</td>
			<td><input type="text" name="NOMOR_WO"></td>
		</tr>
		<tr>
			<td>NOMOR SMU</td>
			<td>:</td>
			<td><input type="text" name="NOMOR_SMU"></td>
		</tr>
		<tr>
			<td>KODE KAPAL</td>
			<td>:</td>
			<td><input type="text" name="KODE_KAPAL"></td>
		</tr>
		<tr>
			<td>KODE ASAL TUJUAN</td>
			<td>:</td>
			<td><input type="text" name="KODE_ASAL_TUJUAN"></td>
		</tr>
		<tr>
			<td>KODE PEMILIK</td>
			<td>:</td>
			<td><input type="text" name="KODE_PEMILIK"></td>
		</tr>
		<tr>
			<td>KODE BARANG</td>
			<td>:</td>
			<td><input type="text" name="KODE_BARANG"></td>
		</tr>
		<tr>
			<td>QTY BARANG</td>
			<td>:</td>
			<td><input type="text" name="QTY_BARANG"></td>
		</tr>
		<tr>
			<td>SATUAN QTY</td>
			<td>:</td>
			<td><input type="text" name="SATUAN_QTY"></td>
		</tr>
		<tr>
			<td>KG BARANG</td>
			<td>:</td>
			<td><input type="text" name="KG_BARANG"></td>
		</tr>
		<tr>
			<td>TARIF</td>
			<td>:</td>
			<td><input type="text" name="TARIF"></td>
		</tr>
		<tr>
			<td>JUMLAH TARIF</td>
			<td>:</td>
			<td><input type="text" name="JML_TARIF"></td>
		</tr>
		<tr>
			<td>STATUS</td>
			<td>:</td>
			<td><input type="text" name="STATUS"></td>
		</tr>
		<tr>
			<td>AUTHOR</td>
			<td>:</td>
			<td><input type="text" name="AUTHOR"></td>
		</tr>
		<tr>
			<td>SET TIME</td>
			<td>:</td>
			<td><input type="text" name="SET_TIME"></td>
		</tr>
		<tr>
			<td>DISCOUNT</td>
			<td>:</td>
			<td><input type="text" name="DISCOUNT"></td>
		</tr>
		<tr>
			<td>PNBP</td>
			<td>:</td>
			<td><input type="text" name="PNBP"></td>
		</tr>
		<tr>
			<td>PEMILIK</td>
			<td>:</td>
			<td><input type="text" name="PEMILIK"></td>
		</tr>
		<tr>
			<td>NAMA BARANG</td>
			<td>:</td>
			<td><input type="text" name="NAMA_BARANG"></td>
		</tr>
		<tr>
			<td>TANGGAL KAPAL</td>
			<td>:</td>
			<td><input type="text" name="TGL_KAPAL"></td>
		</tr>
		<tr>
			<td>NOMOR TELLY</td>
			<td>:</td>
			<td><input type="text" name="NOMOR_TELLY"></td>
		</tr>
		<tr>
			<td>KG TELLY</td>
			<td>:</td>
			<td><input type="text" name="KG_TELLY"></td>
		</tr>
		<tr>
			<td>AUTHOR UPDATE</td>
			<td>:</td>
			<td><input type="text" name="AUTHOR_UPDATE"></td>
		</tr>
		<tr>
			<td>SET TIME UPDATE</td>
			<td>:</td>
			<td><input type="text" name="SET_TIME_UPDATE"></td>
		</tr>
		<tr>
			<td>NOMOR PENERBANGAN</td>
			<td>:</td>
			<td><input type="text" name="NO_PENERBANGAN"></td>
		</tr>
		<tr>
			<td>QTY TELLY</td>
			<td>:</td>
			<td><input type="text" name="QTY_TELLY"></td>
		</tr>
		<tr>
			<td colspan="3"><button type="submit">Update Data</button></td>
		</tr>
	</table>
</body>
</html>