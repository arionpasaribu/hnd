<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="universal admin is super flexible, powerful, clean & modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, universal admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="pixelstrap">
    <link rel="icon" href="<?php echo base_url () ?>template/assets/images/pb.png" type="image/x-icon"/>
    <link rel="shortcut icon" href="<?php echo base_url () ?>template/assets/images/pb.png" type="image/x-icon"/>
    <title>DATA SMU</title>

    <!--Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700" rel="stylesheet">

    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url () ?>template/assets/css/fontawesome.css">

    <!-- ico-font -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url () ?>template/assets/css/icofont.css">

    <!-- Themify icon -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url () ?>template/assets/css/themify.css">

    <!--JSGrid css-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url () ?>template/assets/css/datatables.css">

    <!-- Flag icon -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url () ?>template/assets/css/flag-icon.css">

    <!-- Bootstrap css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url () ?>template/assets/css/bootstrap.css">

    <!-- App css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url () ?>template/assets/css/style.css">

    <!-- Responsive css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url () ?>template/assets/css/responsive.css">

</head>
<body>


<!-- Loader ends -->

<div class="page-wrapper">
    <!--Page Header Start-->
    <?php $this->load->view('templates/header');?>
    <!--Page Header Ends-->
    <div class="page-body-wrapper">
        <!-- Sidebar Start -->
       <?php $this->load->view('templates/sidebar'); ?>
        <!-- Sidebar End -->
        
        <div class="page-body">
            <form action="<?php echo base_url ().'home/tambah_datasmu'; ?>" method="POST">
            <div class="container-fluid">
                <div class="page-header">
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <!--Zero Configuration  Starts -->
                    <div class="col-sm-12">
                    <div class="card-header">
                    <h5>DATA SMU</h5>
                </div><hr>
                <div class="col-sm-4">
                        <div class="form-group">
                            <label for="">NOMOR WO:</label>
                            <select name="nomor_smu" id="" class="form-control">
                            <?php foreach($nomorwo as $l){ ?>
                                <option value="<?php echo $l['NOMOR_WO']; ?>"><?php echo $l['NOMOR_WO']; ?></option>
                            <?php } ?>
                            </select>
                        </div>
                    </div>
                
            <div class="table-responsive table-bordered">
                <table id="basic-1" border="1" class="table-bordered table">
                    <tr>
                        <th colspan="2">NOMOR</th>
                        <th rowspan="2">NAMA BARANG</th>
                        <th rowspan="2">JUMLAH KOLI</th>
                        <th colspan="2">JUMLAH KG</th>
                        <th rowspan="2">HARI</th>
                        <th rowspan="2">PNPB</th>
                        <th rowspan="2">TARIF</th></tr>
                    <td>SMU</td>
                    <td>TELLY</td>
                    <td>PADA SMU</td>
                    <td>DITAGIH</td>
                    <tbody>
                        <td><input type="text" name="smu" class="form-control"></td>
                        <td><input type="text" name="telly" class="form-control"></td>
                        <td><input type="text" name= "nama_barang" class="form-control">
                        <td><input type="text" name="jumlah_koli" class="form-control"></td>
                        <td><input type="text" name="pada_smu" class="form-control"></td>
                        <td><input type="text" name="ditagih" class="form-control"></td>
                        <td><input type="text" name="hari" class="form-control"></td>
                        <td><input type="text" name="pnbp" class="form-control">
                        <td> <select name="tarif" id="" class="form-control">
                            <?php foreach($tarif as $l){ ?>
                                <option value="<?php echo $l['TARIF']; ?>"><?php echo $l['TARIF']; ?></option>
                            <?php } ?>
                            </select></td>
                        
                    </tbody>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        
                    </tr>
                </table>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive table-bordered" >
            <table id="basic-1" border="6" class="table-bordered table"  cellspacing="1" cellpadding="20">

                    <tr>
                        <th rowspan="2" style="text-align: center;">TANGGAL DAN JAM PESAWAT</th>
                        <th colspan="2" style="text-align: center;"> PENERBANGAN </th>
                        <th colspan="2" style="text-align: center;"> ASAL KEDATANGAN</th>
                        <th colspan="2" style="text-align: center;"> PEMILIK BARANG</th>
                    </tr>
                    <td>KODE</td>
                    <td>NAMA PESAWAT</td>
                    <td>NOMOR KODE</td>
                    <td>NAMA KOTA</td>
                    <td>KODE</td>
                    <td>NAMA PEMILIK</td>
                    <tbody>
                        <tr>
                            <td><input type="date" name="tgl_kapal" class="form-control"></td>
                            <td><input type="text" name="no_penerbangan" class="form-control"></td>
                            <td><select name="nama_kapal" id="" class="form-control">
                            <?php foreach($namakapal as $l){ ?>
                                <option value="<?php echo $l['NAMA_KAPAL']; ?>"><?php echo $l['NAMA_KAPAL']; ?></option>
                            <?php } ?>>
                            </select></td>
                            <td><input type="text" name="kode_asal_tujuan" class="form-control"></td>
                            <td><select name="namakota" id="" class="form-control">
                            <?php foreach($namakota as $l){ ?>
                                <option value="<?php echo $l['NAMA_ASAL_TUJUAN']; ?>"><?php echo $l['NAMA_ASAL_TUJUAN']; ?></option>
                            <?php } ?>>
                            </select></td>
                            <td><input type="text" name="pemilik" class="form-control"></td>
                            <td><select name="namacustomer" id="" class="form-control">
                            <?php foreach($namacustomer as $l){ ?>
                                <option value="<?php echo $l['NAMA_CUSTOMER']; ?>"><?php echo $l['NAMA_CUSTOMER']; ?></option>
                            <?php } ?>>
                            </select></td>

                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="container-fluid">
            <input type="submit" value="SAVE" class="btn btn-primary">
        </div>
        
        
        
    </form>

  </body>            
</html>