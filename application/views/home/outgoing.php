<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="universal admin is super flexible, powerful, clean & modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, universal admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="pixelstrap">
    <link rel="icon" href="<?php echo base_url () ?>template/assets/images/pb.png" type="image/x-icon"/>
    <link rel="shortcut icon" href="<?php echo base_url () ?>template/assets/images/pb.png" type="image/x-icon"/>
    <title>Laporan Rekapitulasi Kegiatan Outgoing</title>

    <!--Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700" rel="stylesheet">

    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url () ?>template/assets/css/fontawesome.css">

    <!-- ico-font -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url () ?>template/assets/css/icofont.css">

    <!-- Themify icon -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url () ?>template/assets/css/themify.css">

    <!--JSGrid css-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url () ?>template/assets/css/datatables.css" />

    <!-- Flag icon -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url () ?>template/assets/css/flag-icon.css">

    <!-- Bootstrap css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url () ?>template/assets/css/bootstrap.css">

    <!-- App css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url () ?>template/assets/css/style.css">

    <!-- Responsive css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url () ?>template/assets/css/responsive.css">

</head>
<body>


<!-- Loader ends -->

<div class="page-wrapper">
    <!--Page Header Start-->
    <?php $this->load->view('templates/header');?>
    <!--Page Header Ends--> 
    <div class="page-body-wrapper">
        <!-- Sidebar Start -->
       <?php $this->load->view('templates/sidebar'); ?>
        <!-- Sidebar End -->
        <div class="page-body">
            <div class="container-fluid">
                <div class="page-header">
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <!--Zero Configuration  Starts -->
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-header">
                            </div>
                            <div class="card-body">
                                <h3>LAPORAN REKAPITULASI KEGIATAN OUTGOING</h3>
                                <div class="table-responsive">
                                <table id="basic-1" class="display">
                                <thead>
                                <tr>
                                    <th>JENIS BAYAR</th>
                                    <th>JENIS WO</th>
                                    <th>NAMA BARANG</th>
                                    <th>QTY BARANG</th>
                                    <th>KG BARANG</th>
                                    <th>JUMLAH PNBP</th>
                                    <th>OB</th>
                                    <th>PERSERO</th>
                                    <th>JUMLAH TARIF</th>
                                     </tr>
                                     </thead>
                                     <tbody>
                                       <?php foreach ($outgoing as $key) : ?>
                                   <tr>
                                    <td><?php echo $key['JENIS_BAYAR_'] ?></td>
                                    <td><?php echo $key['JENIS_WO_'] ?></td>
                                    <td><?php echo $key['NAMA_BARANG_'] ?></td>
                                    <td><?php echo number_format(intval($key['QTY_BARANG']),0,",",".") ?></td>
                                    <td><?php echo number_format(intval($key['KG_BARANG']),0,",",".") ?></td>
                                    <td><?php echo number_format(intval($key['JML_PNBP']),0,",",".") ?></td>
                                    <td><?php echo number_format(intval($key['OB']),0,",",".") ?></td>
                                    <td><?php echo number_format(intval($key['PERSERO']),0,",",".") ?></td>
                                    <td><?php echo number_format(intval($key['JML_TARIF']),0,",",".") ?></td>
                                   </tr>
                               <?php endforeach ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Zero Configuration  Ends -->
                </div>
            </div>
        </div>
    </div>
</div>


</body>
</html>