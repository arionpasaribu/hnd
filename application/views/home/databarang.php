<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="universal admin is super flexible, powerful, clean & modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, universal admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="pixelstrap">
    <link rel="icon" href="<?php echo base_url () ?>template/assets/images/pb.png" type="image/x-icon"/>
    <link rel="shortcut icon" href="<?php echo base_url () ?>template/assets/images/pb.png" type="image/x-icon"/>
    <title>DATA BARANG</title>

    <!--Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700" rel="stylesheet">

    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url () ?>template/assets/css/fontawesome.css">

    <!-- ico-font -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url () ?>template/assets/css/icofont.css">

    <!-- Themify icon -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url () ?>template/assets/css/themify.css">

    <!--JSGrid css-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url () ?>template/assets/css/datatables.css" />

    <!-- Flag icon -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url () ?>template/assets/css/flag-icon.css">

    <!-- Bootstrap css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url () ?>template/assets/css/bootstrap.css">

    <!-- App css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url () ?>template/assets/css/style.css">

    <!-- Responsive css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url () ?>template/assets/css/responsive.css">

</head>
<body>


<!-- Loader ends -->

<div class="page-wrapper">
    <!--Page Header Start-->
    <?php $this->load->view('templates/header');?>
    <!--Page Header Ends-->
    <div class="page-body-wrapper">
        <!-- Sidebar Start -->
       <?php $this->load->view('templates/sidebar'); ?>
        <!-- Sidebar End -->
        <div class="page-body">
            <div class="container-fluid">
                <div class="page-header">
                </div>
            </div>
            <div class="container-fluid">
                
                <div class="row">
                    
                    <!--Zero Configuration  Starts -->
                    <div class="col-sm-12">
                        
                        <div class="card">
                            
                            <div class="card-header">
                                <!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
  Tambah Data
</button>
                            </div>
                            <div class="card-body">
                                <h3>DATA BARANG</h3>
                                <div class="table-responsive">
                                <table id="basic-1" class="display">
                                <thead>
                                <tr>
                                    <th>KODE BARANG</th>
                                    <th>NAMA BARANG</th>
                                    <th>SATUAN</th>
                                    <th>KEMASAN</th>
                                    <th>TARIF</th>
                                    <th>PNBP</th>
                                    <th>TARIF DASAR</th>
                                    <th>MATA UANG</th>
                                    <th>GRUP</th>
                                    <th>AKSI</th>

                                     </tr>
                                     </thead>
                                     <tbody>
                                     <?php foreach ($databarang as $key) : ?>
                                   <tr>
                                    <td><?php echo $key->KODE_BARANG ?></td>
                                    <td><?php echo $key->NAMA_BARANG ?></td>
                                    <td><?php echo $key->SATUAN ?></td>
                                    <td><?php echo $key->KEMASAN ?></td>
                                    <td><?php echo $key->TARIF ?></td>
                                    <td><?php echo $key->PNBP ?></td>
                                    <td><?php echo $key->TARIF_DASAR ?></td>
                                    <td><?php echo $key->MATA_UANG ?></td>
                                    <td><?php echo $key->GRUP ?></td>

                                    <td><?php echo anchor('home/edit/'.$key->KODE_BARANG,'<div class="btn btn-primary" data-id="<?php echo $key;->KODE_BARANG ?> value="Update">Update</div>')?></td>
                                </tr>
                               <?php endforeach ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Zero Configuration  Ends -->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Tambah Data Barang</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form  method="post" action="<?php echo base_url ().'home/tambah_data'; ?>">
          <label for ="">KODE BARANG*</label>
          <div class="input-group form-group">
      <span class="input-group-addon" id="sizing-addon2">
        <i class="glyphicon glyphicon-user"></i>
      </span>
      <input type="text" class="form-control" placeholder="Kode Barang" name="kode_barang" aria-describedby="sizing-addon2">
    </div>
    <label for="">NAMA BARANG*</label>
    <div class="input-group form-group">
      <span class="input-group-addon" id="sizing-addon2">
        <i class="glyphicon glyphicon-user"></i>
      </span>
      <input type="text" class="form-control" placeholder="Nama Barang" name="nama_barang" aria-describedby="sizing-addon2">
    </div>
    <label for="">SATUAN*</label>
    <div class="input-group form-group">
      <span class="input-group-addon" id="sizing-addon2">
        <i class="glyphicon glyphicon-user"></i>
      </span>
      <input type="text" class="form-control" placeholder="Satuan" name="satuan" aria-describedby="sizing-addon2">
    </div>
    <label for="">KEMASAN</label>
    <div class="input-group form-group">
      <span class="input-group-addon" id="sizing-addon2">
        <i class="glyphicon glyphicon-user"></i>
      </span>
      <input type="text" class="form-control" placeholder="Kemasan" name="kemasan" aria-describedby="sizing-addon2">
    </div>
    <label for="">TARIF*</label>
    <div class="input-group form-group">
      <label for=""></label>
      <span class="input-group-addon" id="sizing-addon2">
        <i class="glyphicon glyphicon-user"></i>
      </span>
      <input type="text" class="form-control" placeholder="Tarif" name="tarif" aria-describedby="sizing-addon2">
    </div>
    <label for="">PNBP*</label>
    <div class="input-group form-group">
      <span class="input-group-addon" id="sizing-addon2">
        <i class="glyphicon glyphicon-user"></i>
      </span>
      <input type="text" class="form-control" placeholder="Pnbp" name="pnbp" aria-describedby="sizing-addon2">
    </div>
    <label for="">TARIF DASAR*</label>
    <div class="input-group form-group">
      <span class="input-group-addon" id="sizing-addon2">
        <i class="glyphicon glyphicon-user"></i>
      </span>
      <input type="text" class="form-control" placeholder="Tarif Dasar" name="tarifdasar" aria-describedby="sizing-addon2">
    </div>
    <label for="">MATA UANG*</label>
    <div class="input-group form-group">
      <span class="input-group-addon" id="sizing-addon2">
        <i class="glyphicon glyphicon-user"></i>
      </span>
      <input type="text" class="form-control" placeholder="Mata Uang" name="matauang" aria-describedby="sizing-addon2">
    </div>
    <label for="">STATUS*</label>
    <div class="input-group form-group">
      <span class="input-group-addon" id="sizing-addon2">
        <i class="glyphicon glyphicon-user"></i>
      </span>
      <input type="text" class="form-control" placeholder="Status" name="status" aria-describedby="sizing-addon2">
    </div>
    <label for="">GRUP*</label>
    <div class="input-group form-group">
      <span class="input-group-addon" id="sizing-addon2">
        <i class="glyphicon glyphicon-user"></i>
      </span>
      <input type="text" class="form-control" placeholder="Grup" name="grup" aria-describedby="sizing-addon2">
    </div>
      </div>
      <input type="submit" class="btn btn-secondary" value="Simpan">
          </form>
     

      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>


</body>
</html>