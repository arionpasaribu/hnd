<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="universal admin is super flexible, powerful, clean & modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, universal admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="pixelstrap">
    <link rel="icon" href="<?php echo base_url() ?>template/assets/images/pb.png" type="image/x-icon"/>
    <link rel="shortcut icon" href="<?php echo base_url() ?>template/assets/images/pb.png" type="image/x-icon"/>
    <title>Universal - Premium Admin Template</title>

    <!--Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700" rel="stylesheet">

    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>template/assets/css/fontawesome.css">

    <!-- ico-font -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>template/assets/css/icofont.css">

    <!-- Themify icon -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>template/assets/css/themify.css">

    <!--JSGrid css-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>template/assets/css/datatables.css" />

    <!-- Flag icon -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>template/assets/css/flag-icon.css">

    <!-- Bootstrap css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>template/assets/css/bootstrap.css">

    <!-- App css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>template/assets/css/style.css">

    <!-- Responsive css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>template/assets/css/responsive.css">

</head>
<body>


<!-- Loader ends -->

<div class="page-wrapper">
    <!--Page Header Start-->
    <?php $this->load->view('templates/header');?>
    <!--Page Header Ends-->
    <div class="page-body-wrapper">
        <!-- Sidebar Start -->
       <?php $this->load->view('templates/sidebar'); ?>
        <!-- Sidebar End -->
        <div class="page-body">
            <div class="container-fluid">
                <div class="page-header">
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <!--Zero Configuration  Starts -->
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-header">
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="basic-1" class="display">
                                        <thead>
                                        <tr>
                                        <th>NOMOR WO</th>
                                        <th>NOMOR SMU</th>
                                        <th>KODE KAPAL</th>
                                        <th>KODE ASAL TUJUAN</th>
                                        <th>KODE PEMILIK</th>
                                        <th>KODE BARANG</th>
                                        <th>QTY BARANG</th>
                                        <th>SATUAN QTY</th>
                                        <th>KG BARANG</th>
                                        <th>TARIF</th>
                                        <th>JUMLAH TARIF</th>
                                        <th>STATUS</th>
                                        <th>AUTHOR</th>
                                        <th>SET TIME</th>
                                        <th>DISCOUNT</th>
                                        <th>PNBP</th>
                                        <th>PEMILIK</th>
                                        <th>NAMA BARANG</th>
                                        <th>TANGGAL KAPAL</th>
                                        <th>NOMOR TELLY</th>
                                        <th>KG TELLY</th>
                                        <th>AUTHOR UPDATE</th>
                                        <th>SET TIME UPDATE</th>
                                        <th>NO PENERBANGAN</th>
                                        <th>QTY TELLY</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        <?php foreach ($wokjul as $key) : ?>
           <tr>
            <td><?php echo $key['NOMOR_WO'] ?></td>
            <td><?php echo $key['NOMOR_SMU'] ?></td>
            <td><?php echo $key['KODE_KAPAL'] ?></td>
            <td><?php echo $key['KODE_ASAL_TUJUAN'] ?></td>
            <td><?php echo $key['KODE_PEMILIK'] ?></td>
            <td><?php echo $key['KODE_BARANG'] ?></td>
            <td><?php echo $key['QTY_BARANG'] ?></td>
            <td><?php echo $key['SATUAN_QTY'] ?></td>
            <td><?php echo $key['KG_BARANG'] ?></td>
            <td><?php echo $key['TARIF'] ?></td>
            <td><?php echo $key['JML_TARIF'] ?></td>
            <td><?php echo $key['STATUS'] ?></td>
            <td><?php echo $key['AUTHOR'] ?></td>
            <td><?php echo $key['SET_TIME'] ?></td>
            <td><?php echo $key['DISCOUNT'] ?></td>
            <td><?php echo $key['PNBP'] ?></td>
            <td><?php echo $key['PEMILIK'] ?></td>
            <td><?php echo $key['NAMA_BARANG'] ?></td>
            <td><?php echo $key['TGL_KAPAL'] ?></td>
            <td><?php echo $key['NOMOR_TELLY'] ?></td>
            <td><?php echo $key['KG_TELLY'] ?></td>
            <td><?php echo $key['AUTHOR_UPDATE'] ?></td>
            <td><?php echo $key['SET_TIME_UPDATE'] ?></td>
            <td><?php echo $key['NO_PENERBANGAN'] ?></td>
            <td><?php echo $key['QTY_TELLY'] ?></td>
           </tr>
        <?php endforeach ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Zero Configuration  Ends -->
                </div>
            </div>
        </div>
    </div>
</div>


</body>
</html>