<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="universal admin is super flexible, powerful, clean & modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, universal admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="pixelstrap">
    <link rel="icon" href="<?php echo base_url () ?>template/assets/images/pb.png" type="image/x-icon"/>
    <link rel="shortcut icon" href="<?php echo base_url () ?>template/assets/images/pb.png" type="image/x-icon"/>
    <title>EDIT DATA PENERBANGAN</title>

    <!--Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700" rel="stylesheet">

    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url () ?>template/assets/css/fontawesome.css">

    <!-- ico-font -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url () ?>template/assets/css/icofont.css">

    <!-- Themify icon -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url () ?>template/assets/css/themify.css">

    <!--JSGrid css-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url () ?>template/assets/css/datatables.css" />

    <!-- Flag icon -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url () ?>template/assets/css/flag-icon.css">

    <!-- Bootstrap css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url () ?>template/assets/css/bootstrap.css">

    <!-- App css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url () ?>template/assets/css/style.css">

    <!-- Responsive css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url () ?>template/assets/css/responsive.css">

</head>
<body>


<!-- Loader ends -->

<div class="page-wrapper">
    <!--Page Header Start-->
    <?php $this->load->view('templates/header');?>
    <!--Page Header Ends-->
    <div class="page-body-wrapper">
        <!-- Sidebar Start -->
       <?php $this->load->view('templates/sidebar'); ?>
        <!-- Sidebar End -->
        <div class="page-body">
            <div class="container-fluid">
                <div class="page-header">
                </div>
            </div>
            <div class="container-fluid">
                
                <div class="row">
                    
                    <!--Zero Configuration  Starts -->
                    <div class="col-sm-12">
                        
                        <div class="card">
                            
                            <div class="card-header">
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                <table id="basic-1" class="display">
                                <thead>
                                  <?php foreach($datapenerbangan as $data) ?>

                                <form method="POST" action="<?php echo base_url().'home/updatepenerbangan'; ?>" >
                              <div class="form-group">
                                <label>KODE KAPAL</label>
                                <input type="text" disable name="kode_kapal" class="form-control" value="<?php echo $data->KODE_KAPAL?>">
                              </div>
                              <div class="form-group">
                                <label>NAMA KAPAL</label>
                                <input type="text" name="nama_kapal" class="form-control" value="<?php echo $data->NAMA_KAPAL?>">
                              </div>
                              <div class="form-group">
                                <label>BENDERA</label>
                                <input type="text" name="bendera" class="form-control" value="<?php echo $data->BENDERA?>">
                              </div>
                              <div class="form-group">
                                <label>STATUS</label>
                                <input type="text" name="status" class="form-control" value="<?php echo $data->STATUS?>">
                              </div>
                              <div class="form-group">
                                <labl>AUTHOR</labl>
                                <input type="text" name="author" class="form-control" value="<?php echo $data->AUTHOR?>">
                              </div>
                              <div class="form-group">
                                <label>SET TIME</label>
                                <input type="date" name="set_time" class="form-control" value="<?php echo $data->SET_TIME?>">
                              </div>
                              <div class="form-group">
                                <label>INISIAL</label>
                                <input type="text" name="inisial" class="form-control" value="<?php echo $data->INISIAL?>">
                              </div>

                              <input type="submit" class="btn btn-primary" value="Ubah">
                              <input type="reset" class="btn btn-danger" value="Reset">

                              </form>
                              
                              
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Zero Configuration  Ends -->
                </div>
            </div>
        </div>
    </div>
</div>
     

      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>


</body>
</html>