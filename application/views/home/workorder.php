<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="universal admin is super flexible, powerful, clean & modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, universal admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="pixelstrap">
    <link rel="icon" href="<?php echo base_url () ?>template/assets/images/pb.png" type="image/x-icon"/>
    <link rel="shortcut icon" href="<?php echo base_url () ?>template/assets/images/pb.png" type="image/x-icon"/>
    <title>Work Order</title>

    <!--Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700" rel="stylesheet">

    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url () ?>template/assets/css/fontawesome.css">

    <!-- ico-font -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url () ?>template/assets/css/icofont.css">

    <!-- Themify icon -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url () ?>template/assets/css/themify.css">

    <!--JSGrid css-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url () ?>template/assets/css/datatables.css">

    <!-- Flag icon -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url () ?>template/assets/css/flag-icon.css">

    <!-- Bootstrap css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url () ?>template/assets/css/bootstrap.css">

    <!-- App css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url () ?>template/assets/css/style.css">

    <!-- Responsive css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url () ?>template/assets/css/responsive.css">

</head>
<body>


<!-- Loader ends -->

<div class="page-wrapper">
    <!--Page Header Start-->
    <?php $this->load->view('templates/header');?>
    <!--Page Header Ends-->
    <div class="page-body-wrapper">
        <!-- Sidebar Start -->
       <?php $this->load->view('templates/sidebar'); ?>
        <!-- Sidebar End -->
        
        <div class="page-body">
            <form action="<?php echo base_url ().'input/insert_work_order'; ?>" method="POST">
            <div class="container-fluid">
                <div class="page-header">
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <!--Zero Configuration  Starts -->
                    <div class="col-sm-12">
                    <div class="card-header">
                    <h5>WORK ORDER</h5>
                </div><hr>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="">TGL WO:</label>
                            <input type="date" name="tgl_wo" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="">NOMOR WO:</label>
                            <input type="text" name="nomor_wo" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="">DITAGIH PADA:</label>
                            <select name="kode_customer" id="" class="form-control">
                            <?php foreach($customer as $l){ ?>
                                <option value="<?php echo $l['KODE_CUSTOMER']; ?>"><?php echo $l['NAMA_CUSTOMER']; ?>   </option>
                            <?php } ?>
                                
                            </select>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="">KODE JASA:</label>
                            <select name="kode_jasa" id="" class="form-control">
                            <?php foreach($jasa as $l){ ?>
                                <option value="<?php echo $l['KODE_JASA']; ?>"><?php echo $l['NAMA_JASA']; ?>   </option>
                            <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="">PEMBAYARAN:</label>
                            <select name="jenis" id="" class="form-control">
                                <option value="CASH">CASH</option>
                                <option value="KREDIT">KREDIT</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="">NOMOR NOTA:</label>
                            <input type="text" name="nomor_nota" class="form-control">
                        </div>
                    </div>
                </div>
            </div>
        </div>
       
        <div class="container-fluid">
            <input type="submit" value="SAVE" class="btn btn-primary">
        </div>
        
    </form>

  </body>            
</html>